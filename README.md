This project is an assignment of Software Development (Course code CAB302) from QUT Bechelor of Information Technology. 


This assignment involves simulating the bookings for daily flights between the east coast of Australia and the west coast of the United States.


There are two hierarchy of classes in the system:
1. package asgn2Aircraft
2. package assgn2Passengers

Meanwhile developing the system, serveral unit test have to be done simultaneously.


Our group is going to divide the workload into main system development and unit testing as follow:

Toni - system development

Carl - unit testing