/**
 * 
 * This file is part of the AircraftSimulator Project, written as 
 * part of the assessment for CAB302, semester 1, 2016. 
 * 
 */
package asgn2Simulators;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import asgn2Aircraft.AircraftException;
import asgn2Aircraft.Bookings;
import asgn2Passengers.PassengerException;

/**
 * The GUISimulator class generate a windows application for displaying a flight booking system statistic,
 * including 2 charts:
 * 1. the no. of bookings on a daily basis; and
 * 2. a summary of the maximum queued size in a day, the total number of refused passengers, and the
 *    total number of booking seats available.
 * It also provided log as a text area and outputted as a text file.
 * User is allowed to edit the parameter for simulation in the interface.
 * 
 * @author Toni Lam, Carl Mayanja
 * @version 2.0
 * Date: 27th May 2016
 *
 */
@SuppressWarnings("serial")
public class GUISimulator extends JFrame implements Runnable {
	private enum DataLayer {
			CHART1, CHART2, TEXTLOG
	}
	private enum DisplayMode {
			GRAPHIC, TEXT
	}
	
/*****************************************************************************
 *	Attributes Declaration
 */	
		
	// define text used in the GUI
    private static final String
    						TITLE = "Aircraft Booking system",
    						TEXT_BTN_START = "Start Simulation",
    						TEXT_BTN_CHART = "Switch Chart",
    						TEXT_BTN_SWITCH = "Switch Text/Graphic View",
 	    					TEXT_BTN_EXIT = "Exit Program",
    						TEXT_LBL_SEED  = "Seed",
    						TEXT_LBL_QUEUE_SIZE  = "Queue Size",
    						TEXT_LBL_MEAN  = "Mean",
    						TEXT_LBL_STD_DEV = "Standard Deviation",
    						TEXT_LBL_PROB_CANCEL  = "Cancellation",
    						TEXT_LBL_PROB_FIRST = "First",
    						TEXT_LBL_PROB_BUSINESS  = "Business",
    						TEXT_LBL_PROB_PREMIUM  = "Premium",
    						TEXT_LBL_PROB_ECONOMY  = "Economy",
    						TEXT_CHART_X_AXIS  = "Days",
    						TEXT_CHART_Y_AXIS  = "Passengers",
    	    				TEXT_BARCHART_X_AXIS  = "Total",
    	    				TEXT_BARCHART_Y_AXIS  = "Passengers",
    	    				TEXT_LAYER_LINECHART  = "linechart",
    	    				TEXT_LAYER_BARCHART  = "barchart",
    	    				TEXT_LAYER_TEXTLOG  = "textlog",
    						LBL_CHART_FIRST = "First",
    						LBL_CHART_BUSINESS = "Business",
    						LBL_CHART_PREMIUM = "Premium",
    						LBL_CHART_ECONOMY = "Economy",
    						LBL_CHART_TOTAL = "Total",
    						LBL_CHART_AVAILABLE = "Seats available",
    						LBL_CHART_QUEUE = "Queue Size",
    						LBL_CHART_REFUSED = "Passengers Refused",
    						LBL_CHART_CAPACITY = "Daily passenger capacity",
    						TOOLTIP_SEED = "It will affect the random machanism",
							TOOLTIP_STD_DIV = "Standard Deviation is automatic calculated from the mean above";
	
    //define numeric constant for the gui
	public static final int
							WIDTH = 780,
							HEIGHT = 580;
	private static final int
							CHART_AREA_OFFSET_Y = 30,
							INTERACTION_AREA_HEIGHT = 200;
	private static final double
							CONSTANT_SD_FACTOR = 0.33;
	
	private int
		paramSeed,
		paramQueueSize;
	private double
		paramMean,
		paramStdDeviation,
		paramProbCancel,
		paramProbFirst,
		paramProbBusiness,
		paramProbPremium,
		paramProbEconomy;
	
	private JScrollPane pnlDisplayTextLog;
	private JPanel
				pnlInteraction,
				pnlInformation;
	private ChartPanel
				pnlDisplayLine,
				pnlDisplayBar;
	private CardLayout informationlayout;
	
	private JButton
				btnStart,
				btnChart,
				btnSwitch,
				btnExit;

	private JLabel
				lblSeed,
				lblMean,
				lblStdDev,
				lblQueueSize,
				lblProbCancal,
				lblProbFirst,
				lblProbBusiness,
				lblProbPremium,
				lblProbEconomy;
	
	private JTextField
				txtSeed,
				txtMean,
				txtStdDev,
				txtQueueSize,
				txtProbCancal,
				txtProbFirst,
				txtProbBusiness,
				txtProbPremium,
				txtProbEconomy;
	
	private JTextArea fullLog;
	
	private JFreeChart chart;
	private XYSeriesCollection dataset1;

	private JFreeChart barchart;
	private CategoryDataset dataset2;
	
	private String[]
				param,
				infoPanelNames;
	private DataLayer visibleLayer;
	boolean textMode = false;
	
/*
 * END Attributes Declaration
 *****************************************************************************/
 
/*****************************************************************************
 *	Public Methods
 */	
	
	/**
	 * This is the Constructor
	 * @param arg0
	 * @throws HeadlessException (@see Class JFrame)
	 */
	public GUISimulator(String arg0) throws HeadlessException {
		super(arg0);
		if ((arg0 == null) || (arg0.length() == 0)) {
			this.setTitle(TITLE);
		}
		
		param = new String[10];
		param[1] = String.valueOf(Constants.DEFAULT_SEED);
		param[2] = String.valueOf(Constants.DEFAULT_MAX_QUEUE_SIZE);
		param[3] = String.valueOf(Constants.DEFAULT_DAILY_BOOKING_MEAN);
		param[4] = String.valueOf(Constants.DEFAULT_DAILY_BOOKING_SD);
		param[5] = String.valueOf(Constants.DEFAULT_FIRST_PROB);
		param[6] = String.valueOf(Constants.DEFAULT_BUSINESS_PROB);
		param[7] = String.valueOf(Constants.DEFAULT_PREMIUM_PROB);
		param[8] = String.valueOf(Constants.DEFAULT_ECONOMY_PROB);
		param[9] = String.valueOf(Constants.DEFAULT_CANCELLATION_PROB);
	}
	
	/**
	 * This is the Constructor, overloading as input is a array of string.
	 * @param arg0
	 * @throws HeadlessException (@see Class JFrame)
	 */
	public GUISimulator(String[] args) throws HeadlessException {
		super(args[0]);
		if ((args[0] == null) || (args[0].length() == 0)) {
			this.setTitle(TITLE);
		}		
		
		param = args;
	}

	/**
	 * (non-Javadoc)
	 * The run method create all GUI stuffs for the application
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
    	this.createGUI();
		this.setVisible(true);
	}
	
	/**
	 * This main method is used for development testing only.
	 * A proper way to call this GUI is from he SinulationRunner class.
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		SwingUtilities.invokeLater(new GUISimulator("BorderLayout"));
	}
	
/*
 * END Public Methods
 *****************************************************************************/
	
/*****************************************************************************
 *	GUI configuration and management
 */	
	
	/**
	 * This method acts as the core backbone of the whole GUI interface.
	 * All GUI creation processes are managed here.
	 * Panel hierarchy:
	 * GUISimulator
	 * 		- pnlInformation
	 * 			- pnlDisplay
	 * 			- pnlDisplayBar
	 * 			- pnlDisplayTextLog
	 * 		- pnlInteraction
	 */
	private void createGUI() {
		//setup main frame
		setSize(WIDTH, HEIGHT);
		setDefaultLookAndFeelDecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		//setup information sub-panel
		infoPanelNames = new String[] {TEXT_LAYER_LINECHART, TEXT_LAYER_BARCHART, TEXT_LAYER_TEXTLOG};
		pnlDisplayLine = createChartPanel(chart, Color.WHITE);
		pnlDisplayBar = createChartPanel(barchart, Color.WHITE);
		pnlDisplayTextLog = createScrollPanel(Color.LIGHT_GRAY);
		
		//Configure layout for two major panels and add components into them
		createInteractionComponents();
		createInformationComponents();
		pnlInteraction = createPanel(Color.LIGHT_GRAY);
		pnlInformation = createPanel(Color.LIGHT_GRAY);
		layoutInteractionPanel(pnlInteraction);
        layoutInformationPanel(pnlInformation, infoPanelNames);

        //add the two major panel to the frame
		this.getContentPane().add(pnlInformation,BorderLayout.CENTER);
		this.getContentPane().add(pnlInteraction,BorderLayout.SOUTH);
        
		//make the application live
		repaint();
	}

	/**
	 * This method create a ChartPanel to store the JFreeChart instance
	 * and set the background color as given. 
	 * @param chart is the JFreeChart instance to be put on the new panel
	 * @param c is the color of the creating panel
	 * @return new ChartPanel instance
	 */
	private ChartPanel createChartPanel(JFreeChart chart, Color c) {
		ChartPanel cp = new ChartPanel(chart);
		cp.setBackground(c);
		return cp;
	}
	

	/**
	 * This method create a simple JPanel and set the background color as given. 
	 * @param c is the color of the creating panel
	 * @return new JPanel instance
	 */
	private JPanel createPanel(Color c) {
		JPanel jp = new JPanel();
		jp.setBackground(c);
		return jp;
	}
	
	/**
	 * This method create a JScrollPane and set the background color as given. 
	 * @param c is the color of the creating panel
	 * @return new JScrollPane instance
	 */
	private JScrollPane createScrollPanel(Color c) {
		JScrollPane jsp = new JScrollPane();
		jsp.setBackground(c);
		jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jsp.setViewportBorder(new LineBorder(Color.DARK_GRAY));
		return jsp;
	}
	
	/**
	 *  This method create all the GUI components that sit into the information panel.
	 */
	private void createInformationComponents() {
        dataset1 = initialSeriesDataset();
        chart = createChart(dataset1);

        dataset2 = initialCategoryDataset();
        barchart = createBarChart(dataset2);
        
		fullLog = new JTextArea("", 22, 40);
	}
	
	/**
	 * This method create all the GUI components that sit into the interaction panel.
	 */
	private void createInteractionComponents() {
		int paramCounter = 0;
		// create input fields and their labels
		
		++paramCounter;
		lblSeed = createLabel(TEXT_LBL_SEED);
		txtSeed = createTextField(param[paramCounter]);
		
		++paramCounter;
		lblQueueSize = createLabel(TEXT_LBL_QUEUE_SIZE);
		txtQueueSize = createTextField(param[paramCounter]);
		
		++paramCounter;
		lblMean = createLabel(TEXT_LBL_MEAN);
		txtMean = createTextField(param[paramCounter]);
		txtMean.getDocument().addDocumentListener(new TextfieldListener());
		
		++paramCounter;
		lblStdDev = createLabel(TEXT_LBL_STD_DEV);
		txtStdDev = createTextField(param[paramCounter]);
		txtStdDev.setEditable(false);
		txtStdDev.setForeground(Color.blue);
		
		++paramCounter;
		lblProbFirst = createLabel(TEXT_LBL_PROB_FIRST);
		txtProbFirst = createTextField(param[paramCounter]);
		
		++paramCounter;
		lblProbBusiness = createLabel(TEXT_LBL_PROB_BUSINESS);
		txtProbBusiness = createTextField(param[paramCounter]);
		
		++paramCounter;
		lblProbPremium = createLabel(TEXT_LBL_PROB_PREMIUM);
		txtProbPremium = createTextField(param[paramCounter]);
		
		++paramCounter;
		lblProbEconomy = createLabel(TEXT_LBL_PROB_ECONOMY);
		txtProbEconomy = createTextField(param[paramCounter]);
		
		++paramCounter;
		lblProbCancal = createLabel(TEXT_LBL_PROB_CANCEL);
		txtProbCancal = createTextField(param[paramCounter]);
		
		// create control buttons
		btnStart = createButton(TEXT_BTN_START);
		btnChart = createButton(TEXT_BTN_CHART);
		btnSwitch = createButton(TEXT_BTN_SWITCH);
		btnExit = createButton(TEXT_BTN_EXIT);
	}
	
	/**
	 * This method attaches a card layout to the given panel,
	 * and add the related sub-panel into the layout.
	 * @param pnl
	 */
	private void layoutInformationPanel(JPanel pnl, String[] layerName) {
		int half = 2;
		CardLayout layout = new CardLayout();
	    layout.setHgap(10);
	    layout.setVgap(10);
		pnl.setLayout(layout);
		
		pnlDisplayLine.add(new ChartPanel(chart,
								WIDTH - CHART_AREA_OFFSET_Y, HEIGHT - INTERACTION_AREA_HEIGHT,
								WIDTH / half, HEIGHT / half,
								WIDTH, HEIGHT,
								false, false, false, false, false, false),
							BorderLayout.CENTER);
		pnlDisplayBar.add(new ChartPanel(barchart,
								WIDTH - CHART_AREA_OFFSET_Y, HEIGHT - INTERACTION_AREA_HEIGHT,
								WIDTH / half, HEIGHT / half,
								WIDTH, HEIGHT,
								false, false, false, false, false, false),
							BorderLayout.CENTER);
        pnlDisplayTextLog.getViewport().add(fullLog, null);
        
        pnl.add(layerName[0], pnlDisplayLine);
        pnl.add(layerName[1], pnlDisplayBar);
        pnl.add(layerName[2], pnlDisplayTextLog);
        
		visibleLayer = DataLayer.CHART2;
		switchInformation(visibleLayer, DisplayMode.GRAPHIC);
	}
	
	/**
	 * This method attaches a  GridBagLayout to the given panel
	 * @param pnl
	 */
	private void layoutInteractionPanel(JPanel pnl) {
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		int thisRow = 0,
				thisCol = 0,
				componentWidth = 1,
				componentHeight = 1;

		// layout and constraints setup
		pnl.setLayout(layout);
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 100;
		constraints.weighty = 100;
		
		// start putting stuffs at first column
		thisCol = 0;
		thisRow = 0;
		addToPanel(pnl, lblSeed, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, lblMean, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, lblStdDev, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, lblQueueSize, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, lblProbCancal, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		
		// putting stuffs at 2nd column
		thisCol++;
		thisRow = 0;
		addToPanel(pnl, txtSeed, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		txtSeed.setToolTipText(TOOLTIP_SEED);
		addToPanel(pnl, txtMean, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, txtStdDev, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		txtStdDev.setToolTipText(TOOLTIP_STD_DIV);
		addToPanel(pnl, txtQueueSize, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, txtProbCancal, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		
		// putting stuffs at 3rd column
		thisCol++;
		thisRow = 0;
		addToPanel(pnl, lblProbFirst, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, lblProbBusiness, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, lblProbPremium, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, lblProbEconomy, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		
		// putting stuffs at 4th column
		thisCol++;
		thisRow = 0;
		addToPanel(pnl, txtProbFirst, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, txtProbBusiness, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, txtProbPremium, constraints, thisCol, thisRow++, componentWidth, componentHeight);
		addToPanel(pnl, txtProbEconomy, constraints, thisCol, thisRow++, componentWidth, componentHeight);

		// putting stuffs at 5th column
		thisCol++;
		thisRow = 0;
		addToPanel(pnl, btnStart,constraints, thisCol, thisRow++,1,1);
		addToPanel(pnl, btnChart,constraints, thisCol, thisRow++,1,1);
		addToPanel(pnl, btnSwitch,constraints, thisCol,thisRow++,1,1);
		addToPanel(pnl, btnExit,constraints, thisCol, thisRow++,1,1);
		btnChart.setEnabled(false);
		btnSwitch.setEnabled(false);
	}
	
	/**
	 * This method acts as a helper of the layoutInteractionPanel() method.
	 * The given component will be added to the given panel with the constraint parameters
	 * @param jp is the JPanel that attached to
	 * @param c is the component that will be added to the JPanel
	 * @param constraints is a GridBagConstraints object
	 * @param x is the column position of the layout
	 * @param y is the row position of the layout
	 * @param w is the width of the component
	 * @param h is the height of the component
	 */
	private void addToPanel(JPanel jp,Component c, GridBagConstraints constraints,int x, int y, int w, int h) {
		constraints.gridx = x;
		constraints.gridy = y;
		constraints.gridwidth = w;
		constraints.gridheight = h;
		jp.add(c, constraints);
	}
	
	/**
	 * This method is used to switch different chart and log panel in the major information panel.
	 * @param dLayer
	 * @param mode
	 */
	private void switchInformation(DataLayer dLayer, DisplayMode mode) {
		informationlayout = (CardLayout)(pnlInformation.getLayout());
		switch (mode) {
			case TEXT:
				switch (dLayer) {
					case CHART1:
						if (textMode) {
							informationlayout.show(pnlInformation, infoPanelNames[0]);
							textMode = false;
							btnChart.setEnabled(true);
						} else {
							informationlayout.show(pnlInformation, infoPanelNames[2]);
							textMode = true;
							btnChart.setEnabled(false);
						}
						break;
					default:
						// DataLayer.CHART2 is the default data layer
						if (textMode) {
							informationlayout.show(pnlInformation, infoPanelNames[1]);
							textMode = false;
							btnChart.setEnabled(true);
						} else {
							informationlayout.show(pnlInformation, infoPanelNames[2]);
							textMode = true;
							btnChart.setEnabled(false);
						}
						break;
				}
				break;
			default:
				// DisplayMode.GRAPHIC is the default mode
				textMode = false;
				switch (dLayer) {
					case CHART1:
						informationlayout.show(pnlInformation, infoPanelNames[1]);
						visibleLayer = DataLayer.CHART2;
						break;
					default:
						informationlayout.show(pnlInformation, infoPanelNames[0]);
						visibleLayer = DataLayer.CHART1;
						break;
				}
				break;
		}
	}
	
/*
 * END GUI configuration and management
 *****************************************************************************/	
	
/*****************************************************************************
 *	General GUI components configuration
 */	
	
	/**
	 * Create a new JButton object.
	 * @param str is the text appears on the button.
	 * @return the new JButton instance.
	 */
	private JButton createButton(String str) {
		JButton newButton = new JButton(str);
		newButton.addActionListener(new ButtonListener());
		newButton.setName(str);
		return newButton;
	}

	/**
	 * Create a new JLabel object.
	 * @param str is the text appears on the label.
	 * @return the new JLabel instance.
	 */
	private JLabel createLabel(String str) {
		//Create a JButton object and store it in a local var
		//Set the button text to that passed in str
		//Add the frame as an actionListener
		//Return the JButton object
		JLabel newLabel= new JLabel(str, SwingConstants.RIGHT);
		return newLabel;
	}

	/**
	 * Create a new JTextField object.
	 * @param str is the value appears in the TextField
	 * @return the new JTextField instance.
	 */
	private JTextField createTextField(String str) {
		JTextField newTextField= new JTextField(str);
		Border textAreaBorder = BorderFactory.createLoweredBevelBorder();
		newTextField.setBorder(textAreaBorder);
		return newTextField;
	}
	
/*
 * General GUI components configuration
 *****************************************************************************/	

/*****************************************************************************
 *	JFreeChart Configuration	
 */	
		
	/**
	 * This method create the XYSeries lines that plot on the graph.
	 * @return XYSeriesCollection that contains all the XYSeries.
	 */
	private XYSeriesCollection initialSeriesDataset() {
		XYSeriesCollection sCollection = new XYSeriesCollection();
		XYSeries  bookTotal = new XYSeries(LBL_CHART_TOTAL, false, false);
		XYSeries  firTotal = new XYSeries(LBL_CHART_FIRST, false, false); 
		XYSeries  busTotal = new XYSeries(LBL_CHART_BUSINESS, false, false);
		XYSeries  premTotal = new XYSeries(LBL_CHART_PREMIUM, false, false); 
		XYSeries  econTotal = new XYSeries(LBL_CHART_ECONOMY, false, false);
		XYSeries  availableTotal = new XYSeries(LBL_CHART_AVAILABLE, false, false);
		
		sCollection.addSeries(bookTotal);
		sCollection.addSeries(firTotal);
		sCollection.addSeries(busTotal);
		sCollection.addSeries(premTotal);
		sCollection.addSeries(econTotal);
		sCollection.addSeries(availableTotal);
		
		return sCollection;
	}

	/**
	 * This method create the CategoryDataset for the bar chart.
	 * @return CategoryDataset that contains all the categories data.
	 */
	private CategoryDataset initialCategoryDataset() {
		DefaultCategoryDataset cat = new DefaultCategoryDataset();
		String groupName = "Type";
		cat.addValue(0, LBL_CHART_QUEUE, groupName);
		cat.addValue(0, LBL_CHART_REFUSED, groupName);
		cat.addValue(0, LBL_CHART_CAPACITY, groupName);

		return cat;
	}

	/**
	 * This method create the XYLineChart that shows the daily booking status from simulator. 
	 * @param dataset is the data that will appear in the chart
	 * @return JFreeChart that is a XYLineChart to show daily booking status
	 */
    private JFreeChart createChart(final XYDataset dataset) {
        final JFreeChart result = ChartFactory.createXYLineChart(
            TITLE, TEXT_CHART_X_AXIS, TEXT_CHART_Y_AXIS, dataset);
        final XYPlot plot = result.getXYPlot();
        
        ValueAxis domain = plot.getDomainAxis();
        domain.setAutoRange(true);
        ValueAxis range = plot.getRangeAxis();
        range.setAutoRange(true);
        setLineChartColor(result, dataset);
        range.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        return result;
    }
    
    /**
     * This method create the BarChart that shows the summary of daily booking status from simulator.  
     * @param dataset is the data that will appear in the chart
     * @return JFreeChart that is a BarChart to show the summary of the booking
     */
    private JFreeChart createBarChart(final CategoryDataset dataset) {
        final JFreeChart result = ChartFactory.createBarChart(
            TITLE, TEXT_BARCHART_X_AXIS, TEXT_BARCHART_Y_AXIS, dataset);
        final CategoryPlot plot = result.getCategoryPlot();
        
        ValueAxis range = plot.getRangeAxis();
        range.setAutoRange(true);
        setBarChartColor(result, dataset);
        return result;
    }
    
    /**
     * This method assigns colors to the chart's lines 
     * @param chart is the chart going to paint
     * @param ds is the dataset that will change each of the color
     */
	private void setLineChartColor(JFreeChart chart, XYDataset ds) {
		XYPlot plot = (XYPlot) chart.getPlot();
		XYLineAndShapeRenderer r1 = new XYLineAndShapeRenderer();
		int i = 0;
		
		if (ds instanceof XYSeriesCollection) {
			while (i < ds.getSeriesCount()) {
	        	if (ds.getSeriesKey(i) == LBL_CHART_FIRST) {
			        r1.setSeriesPaint(i, Color.black); 
			        r1.setSeriesShapesVisible(i,  false);
	        	}
	        	if (ds.getSeriesKey(i) == LBL_CHART_BUSINESS) {
			        r1.setSeriesPaint(i, Color.blue); 
			        r1.setSeriesShapesVisible(i,  false);
	        	}
	        	if (ds.getSeriesKey(i) == LBL_CHART_PREMIUM) {
			        r1.setSeriesPaint(i, Color.cyan); 
			        r1.setSeriesShapesVisible(i,  false);
	        	}
	        	if (ds.getSeriesKey(i) == LBL_CHART_ECONOMY) {
			        r1.setSeriesPaint(i, Color.gray); 
			        r1.setSeriesShapesVisible(i,  false);
	        	}
	        	if (ds.getSeriesKey(i) == LBL_CHART_TOTAL) {
			        r1.setSeriesPaint(i, Color.green); 
			        r1.setSeriesShapesVisible(i,  false);
	        	}
	        	if (ds.getSeriesKey(i) == LBL_CHART_AVAILABLE) {
			        r1.setSeriesPaint(i, Color.red); 
			        r1.setSeriesShapesVisible(i,  false);
	        	}
	        	
	        	++i;
	        }
		}
        plot.setDataset(0, ds);
        plot.setRenderer(0, r1);
	}

	/**
     * This method assigns colors to the chart's bar 
     * @param chart is the chart going to paint
     * @param ds is the dataset that will change each of the color
     */
	private void setBarChartColor(JFreeChart chart, CategoryDataset ds) {
		final CategoryPlot plot = chart.getCategoryPlot();
        plot.setNoDataMessage("NO DATA!");
        
		final CategoryItemRenderer r2 = new BarRenderer();
		r2.setSeriesPaint(0, Color.black);
		r2.setSeriesPaint(1, Color.red);
		r2.setSeriesPaint(2, Color.green);
		plot.setDataset(0, ds);
        plot.setRenderer(0, r2);
	}
	
/*
 * END JFreeChart Configuration
 *****************************************************************************/	
    
/*****************************************************************************
 *	Processing Data
 */	
    
	/**
	 * This method transform the text fields value to usable variable and throws exception if the data is not
	 * in its relevant type.
	 * 
	 * @throws NullPointerException
	 * @throws NumberFormatException
	 * @throws SimulationException
	 */
    private void scanInput() throws NullPointerException, NumberFormatException, SimulationException {
    	List<String> errNames = new ArrayList<String>();
    	String completeErrMsg = "";
    	
    	// transform the text fields value to class variable
    	paramSeed = Integer.parseInt(txtSeed.getText());
    	paramMean = Double.parseDouble(txtMean.getText());
    	paramStdDeviation = Double.parseDouble(txtStdDev.getText());
    	paramQueueSize = Integer.parseInt(txtQueueSize.getText());
    	paramProbCancel = Double.parseDouble(txtProbCancal.getText());
    	paramProbFirst = Double.parseDouble(txtProbFirst.getText());
    	paramProbBusiness = Double.parseDouble(txtProbBusiness.getText());
    	paramProbPremium = Double.parseDouble(txtProbPremium.getText());
    	paramProbEconomy = Double.parseDouble(txtProbEconomy.getText());
    	
    	// building the error message of invalid value of probability
    	if (invalidProbability(paramProbCancel)){
    		errNames.add(TEXT_LBL_PROB_CANCEL);
    	}
    	if (invalidProbability(paramProbFirst)){
    		errNames.add(TEXT_LBL_PROB_FIRST);
    	}
    	if (invalidProbability(paramProbBusiness)){
    		errNames.add(TEXT_LBL_PROB_BUSINESS);
    	}
    	if (invalidProbability(paramProbPremium)){
    		errNames.add(TEXT_LBL_PROB_PREMIUM);
    	}
    	if (invalidProbability(paramProbEconomy)){
    		errNames.add(TEXT_LBL_PROB_ECONOMY);
    	}
    	if (errNames.size() > 0) {
    		completeErrMsg += "Probability of " + String.join(", ", errNames) + " must lie in [0,1].\n";
    	}
    	
    	// building the error message of invalid total probability (should be equals to 1.0)
    	if ((paramProbFirst + paramProbBusiness + paramProbPremium + paramProbEconomy) > 1) {
    		completeErrMsg += "Total probabilities of the four classes should not be greater than 1.\n";
    	}

    	// building the error message of invalid value of mean, max. queue size and standard deviation
    	errNames.clear();
    	if (paramMean < 0) {
    		errNames.add(TEXT_LBL_MEAN);
    	}
    	if (paramQueueSize < 0) {
    		errNames.add(TEXT_LBL_QUEUE_SIZE);
    	}
    	if (paramStdDeviation < 0) {
    		errNames.add(TEXT_LBL_STD_DEV);
    	}
    	if (errNames.size() > 0) {
    		completeErrMsg += "Invalid " + String.join(", ", errNames) + ".\n";
    	}
    	
    	// throw out an exception if the error message has something built.
    	if (completeErrMsg.length() > 0 ) {
			throw new SimulationException(completeErrMsg);
    	}
    }
    
    /**
     * A helper method for scanInput() method to validate probability value.
     * @param prob is the probability value need to validate
     * @return true if the probability is not acceptable
     * 		   false if the probability is accepted
     */
    private boolean invalidProbability(double prob) {
		return (prob < 0.0) || (prob > 1.0);
	}

    /**
     * This method appends daily record to the LineChart.
     * 
     * @param dayCounter is the day number (X-coordinate) to be added to the chart
     * @param ds is the dataset of the current line chart XYSeries
     * @param values is the value set to be added in this particular day
     * @throws SimulationException if the no. of value is not the same as the amount of series
     */
	private void updateBookingsPerDay(Integer dayCounter, XYSeriesCollection ds, HashMap<String, Integer> values) throws SimulationException {
		if (ds.getSeriesCount() != values.size()) {
			throw new SimulationException("Number of key & value not match ["+ds.getSeriesCount() + "," + values.size()+"].");
		}
		for (String keyName : values.keySet()) {
			ds.getSeries(keyName).add(dayCounter, values.get(keyName));
		}
	}
	
	/**
	 * This method is to update the whole line chart by the entire booking records from simulation 
	 * 
	 * @param ds is the dataset of the current line chart XYSeries
	 * @param bks is the booking records from simulation
	 * @throws SimulationException if there is error on adding data to a day (@see updateBookingsPerDay()) 
	 */
	private void updateBookingsDataset(XYSeriesCollection ds, List<Bookings> bks) throws SimulationException {
		HashMap<String, Integer> values = new HashMap<String, Integer>();
		int dayCounter = 0;
		Iterator<Bookings> iterBks = bks.iterator();
		
		// adding the booking records day by day
		while (iterBks.hasNext()) {			
			Bookings currentDayBooking = iterBks.next();
			values.put(LBL_CHART_TOTAL, currentDayBooking.getNumFirst()
										+ currentDayBooking.getNumBusiness()
										+ currentDayBooking.getNumPremium()
										+ currentDayBooking.getNumEconomy()); 
			values.put(LBL_CHART_FIRST, currentDayBooking.getNumFirst()); 
			values.put(LBL_CHART_BUSINESS, currentDayBooking.getNumBusiness()); 
			values.put(LBL_CHART_PREMIUM, currentDayBooking.getNumPremium()); 
			values.put(LBL_CHART_ECONOMY, currentDayBooking.getNumEconomy()); 
			values.put(LBL_CHART_AVAILABLE, currentDayBooking.getAvailable());
	        
			updateBookingsPerDay(dayCounter, ds, values);
			++dayCounter;
		}
	}	

	
	/**
	 * This method is to update the whole bar chart by the data provided by the caller method (@see updateDailyRecords())
	 * 
	 * @param inds is the incoming dataset related to the bar chart
	 * @param maxQueueSize is the value of the max. queue size bar
	 * @param numRefused is the value of the number of refused passengers bar
	 * @param totalDailyCapacity is the value of total daily capacity bar
	 */
	private void updateSummaryDataset(CategoryDataset inds, int maxQueueSize, int numRefused,
			int totalDailyCapacity) {
		DefaultCategoryDataset cds = (DefaultCategoryDataset) inds;
		String groupName = "Type";
		
		// adding the data records day by day
		cds.setValue(maxQueueSize, LBL_CHART_QUEUE, groupName);
		cds.setValue(numRefused, LBL_CHART_REFUSED, groupName);
		cds.setValue(totalDailyCapacity, LBL_CHART_CAPACITY, groupName);
	}
	
	/**
	 * This method update the entire line chart by a re-generate a new set of simulation data.
	 * 
	 * @throws SimulationException when error occurs in updating chart (@see updateBookingsDataset)
	 * @throws IOException when logging has problem (@see Log.class)
	 * @throws AircraftException when there is any problem running the simulator
	 * @throws PassengerException when there is any problem running the simulator
	 */
	private void updateDailyRecords() throws SimulationException, IOException, AircraftException, PassengerException {
		Log log = new Log(); 
		List<Bookings> bks = new ArrayList<Bookings>();
		String displayPlainText = "";
		Simulator sim = new Simulator(
				paramSeed,
		    	paramQueueSize,
		    	paramMean,
		    	paramStdDeviation,
		    	paramProbFirst,
		    	paramProbBusiness,
		    	paramProbPremium,
		    	paramProbEconomy,
		    	paramProbCancel
		    );
		String logtime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		int currentQueueSize = 0,
			maxQueueSize = 0,
			totalDailyCapacity = 0;
		
		// clear the records from the chart if is has been drawn
		for (Object series : dataset1.getSeries()){
			XYSeries a = (XYSeries) series;
			if (a.getItemCount() > 1) {
				a.delete(0, a.getItemCount() - 1);
			}
		}

		// initialize the log history for the textlog in GUI
		fullLog.setText("");
		
		// Simulation loop start here:
		displayPlainText += logtime + ": Start of Simulation\n";
		sim.createSchedule();
		log.initialEntry(sim);
		displayPlainText += sim.getFlights(Constants.FIRST_FLIGHT).initialState();
		
		// simulate from day to day
		for (int time = 0; time<=Constants.DURATION; time++) {
			sim.resetStatus(time); 
			sim.rebookCancelledPassengers(time); 
			sim.generateAndHandleBookings(time);
			sim.processNewCancellations(time);
			if (time >= Constants.FIRST_FLIGHT) {
				sim.processUpgrades(time);
				sim.processQueue(time);
				sim.flyPassengers(time);
				sim.updateTotalCounts(time); 
				bks.add( sim.getFlightStatus(time) );
				currentQueueSize = sim.numInQueue();
				if (currentQueueSize > maxQueueSize) {
					maxQueueSize = currentQueueSize;
				}
				log.logFlightEntries(time, sim);
			} else {
				sim.processQueue(time);
			}
			log.logQREntries(time, sim);
			log.logEntry(time, sim);
			displayPlainText += sim.getSummary(time, (time >= Constants.FIRST_FLIGHT));
		}
		
		// finalize everything at the end
		sim.finaliseQueuedAndCancelledPassengers(Constants.DURATION); 
		totalDailyCapacity = sim.getFlightStatus(Constants.FIRST_FLIGHT).getTotal();
		displayPlainText += "\n" + logtime + ": End of Simulation\n";
		displayPlainText += sim.finalState();
		log.logQREntries(Constants.DURATION, sim);
		log.finalise(sim);
		
		// using the generated data to draw the charts and write text log on GUI
		updateBookingsDataset(dataset1, bks);
		updateSummaryDataset(dataset2, maxQueueSize, sim.numRefused(), totalDailyCapacity);
		fullLog.setText(displayPlainText);
	}
	
/*
 * END Processing Data
 *****************************************************************************/	

/*****************************************************************************
 *	Create listener
 */	
	
	/**
	 * ButtonListener class is used to listen to the button click event in the interaction panel.
	 * @author Toni Lam
	 *
	 */
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JButton eventBtn = (JButton) e.getSource();
			if (eventBtn.getName().equals(TEXT_BTN_START)) {
				// Start to generate a new set of data from simulation.
				try {
					scanInput();
					updateDailyRecords();
					switchInformation(DataLayer.CHART2, DisplayMode.GRAPHIC);
					
					// enable user to choose the report and display mode after drawn the graphs
					btnChart.setEnabled(true);
					btnSwitch.setEnabled(true);
				} catch (NumberFormatException err) {
					// show error if the scanInput() method cannot transform the input to numeric value 
					JOptionPane.showMessageDialog(null, err.getMessage() + 
							"\nSeed and Queue Size must be integers and all other must be decimal number.");
				} catch (SimulationException err) {
					// show other error that known when doing transform
					JOptionPane.showMessageDialog(null, err.getMessage());
				} catch (Exception err) {
					// catch other unknown exception, just in case
					err.printStackTrace();
					JOptionPane.showMessageDialog(null, "System Error.");
				}
			} else if (eventBtn.getName().equals(TEXT_BTN_SWITCH)) {
				// switch between graphical chart and text log
				switchInformation(visibleLayer, DisplayMode.TEXT);
			} else if (eventBtn.getName().equals(TEXT_BTN_CHART)) {
				// switch between line chart and bar chart
				switchInformation(visibleLayer, DisplayMode.GRAPHIC);
			} else if (eventBtn.getName().equals(TEXT_BTN_EXIT)) {
				// Exit the application gracefully
				System.exit(0);
			}
		}
	}
	
	/**
	 * TextfieldListener class is used to listen to the text field value change.
	 * if the txtMean field is being updated, txtStdDev which holds the standard deviation will
	 * be updated by the mean value automatically.
	 * @author Toni Lam
	 *
	 */
	private class TextfieldListener implements DocumentListener  {

		@Override
		public void insertUpdate(DocumentEvent e) {
			updateValue();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			updateValue();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			updateValue();
		}
		
		/**
		 * Simple private method for the TextfieldListener class to calculate the standard deviation
		 * from the mean.
		 * Precondition: txtMean not null
		 * Postcondition: txtStdDev value changed if the value of txtMean can be convert to double value;
		 * 				  txtStdDev shows #ERR to indicate error .
		 */
		private void updateValue() {
            DecimalFormat df2 = new DecimalFormat("###.##");
            
			try {
				paramStdDeviation = Double.valueOf(df2.format(
										Double.parseDouble(txtMean.getText()) * CONSTANT_SD_FACTOR
									));
				txtStdDev.setText(String.valueOf(paramStdDeviation));
			} catch (Exception err) {
				txtStdDev.setText("#ERR");
			}
		}

	}
	
/*
 * END Create listener
 *****************************************************************************/	
	
}
