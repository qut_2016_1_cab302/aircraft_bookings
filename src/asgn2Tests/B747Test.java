package asgn2Tests;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import asgn2Aircraft.AircraftException;
import asgn2Aircraft.B747;
import asgn2Simulators.Constants;

public class B747Test {
    String flightCode ="fb";
    int bookingTime = Constants.FIRST_FLIGHT + 1;
    int departureTime = bookingTime + 9;
	int first = 12;
	int business = 54;
	int premium = 27;
	int economy = 260;
    
    @Test
    public void defaultFareconstructorShouldSetTypeAndCapacity() throws AircraftException {
        B747 b747 = new B747(flightCode, departureTime);
        
        // assert counter initial state
        assertEquals(b747.initialState(), "B747:" + flightCode + ":" + departureTime 
        								  + " Capacity: 353 [F: 14 J: 52 P: 32 Y: 255]");
        
        // assert counter initial state
        assertEquals(0, b747.getNumFirst());
        assertEquals(0, b747.getNumBusiness());
        assertEquals(0, b747.getNumPremium());
        assertEquals(0, b747.getNumEconomy());
        assertEquals(0, b747.getNumPassengers());
        assertEquals(0, b747.getPassengers().size());
    }
    
    @Test
    public void allFaresConstructorShouldSetTypeAndCapacity() throws AircraftException {
        int totalCapacity = first + business + premium + economy;
        B747 b747 = new B747(flightCode, departureTime,first, business, premium, economy);
        
        // assert counter initial state
        assertEquals(b747.initialState(), "B747:" + flightCode + ":" + departureTime
        								  + " Capacity: " + totalCapacity
        								  + " [F: " + first + " J: " + business
        								  + " P: " + premium + " Y: " + economy + "]");
        
        // assert counter initial state
        assertEquals(0, b747.getNumFirst());
        assertEquals(0, b747.getNumBusiness());
        assertEquals(0, b747.getNumPremium());
        assertEquals(0, b747.getNumEconomy());
        assertEquals(0, b747.getNumPassengers());
        assertEquals(0, b747.getPassengers().size());
    }
}
