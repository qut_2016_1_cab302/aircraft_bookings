package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Aircraft.AircraftException;

public class AircraftExceptionTest{

    @Test
    public void exceptionShouldSetMessage() {
    	String exceptionStartWith = "Aircraft Exception: ";
    	String exceptionendWith = "msj";
    	AircraftException ae = new AircraftException("msj");
        String errorMessage = ae.getMessage();
        
        assertEquals(exceptionStartWith,
        		errorMessage.substring(0,
        								  exceptionStartWith.length()));
        assertEquals(exceptionendWith,
        		errorMessage.substring(errorMessage.length() - exceptionendWith.length(),
        								errorMessage.length()));
    }
}
