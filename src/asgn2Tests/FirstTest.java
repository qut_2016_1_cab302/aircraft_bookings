package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

public class FirstTest{
    int bookingTime = 2;
    int departureTime = 5;
    
    @Test
    public void constructorShouldSetBookingTimeDepartureTimeAndPassID() throws PassengerException {
    	Passenger first = new First(bookingTime, departureTime);
        
        assertEquals(bookingTime, first.getBookingTime());
        assertEquals(departureTime, first.getDepartureTime());
        assertEquals("F", first.getPassID().substring(0, 1));
    }
    
    @Test
    public void noSeatsMessageShouldIncludeFirst() throws PassengerException {
        Passenger first = new First(bookingTime, departureTime);
        
        assertEquals("No seats available in First", first.noSeatsMsg());
    }
    
    @Test
    public void upgradeShouldReturnFirstClassPassenger() throws PassengerException {
    	Passenger p = new First(bookingTime, departureTime);
        p.upgrade();
        
        assertTrue(p instanceof First);
    }
}
