package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Aircraft.A380;
import asgn2Aircraft.Aircraft;
import asgn2Aircraft.AircraftException;
import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;
import asgn2Simulators.Constants;

public class A380Test {
    String flightCode ="fc";
    int bookingTime = Constants.FIRST_FLIGHT + 1;
	int confirmationTime = Constants.FIRST_FLIGHT + 2;
	int queueTime = Constants.FIRST_FLIGHT + 3;
    int cancellationTime = confirmationTime + 4;
    int departureTime = bookingTime + 9;
    int first = 20;
    int business = 10;
    int premium = 5;
    int economy = 100;
	
    @Test
    public void defaultFareconstructorShouldSetTypeAndCapacity() throws AircraftException {
        Aircraft a380 = new A380(flightCode, departureTime);
        
        // assert initial capacity
        assertEquals(a380.initialState(), "A380:" + flightCode + ":" + departureTime 
        								  + " Capacity: 484 [F: 14 J: 64 P: 35 Y: 371]");
        
        // assert counter initial state
        assertEquals(0, a380.getNumFirst());
        assertEquals(0, a380.getNumBusiness());
        assertEquals(0, a380.getNumPremium());
        assertEquals(0, a380.getNumEconomy());
        assertEquals(0, a380.getNumPassengers());
        assertEquals(0, a380.getPassengers().size());
    }
    
    @Test
    public void constructorWithCorrectValuesShouldInitializeFields() throws AircraftException {
        int totalCapacity = first + business + premium + economy;
        Aircraft a380 = getAircratfInstance();
        
        // assert initial capacity
        assertEquals(a380.initialState(), "A380:" + flightCode + ":" + departureTime 
        								  + " Capacity: " + totalCapacity
        								  + " [F: " + first + " J: " + business
        								  + " P: " + premium + " Y: " + economy + "]");
        
        // assert counter initial state
        assertEquals(0, a380.getNumFirst());
        assertEquals(0, a380.getNumBusiness());
        assertEquals(0, a380.getNumPremium());
        assertEquals(0, a380.getNumEconomy());
        assertEquals(0, a380.getNumPassengers());
    }
    
    @Test(expected=AircraftException.class)
    public void constructorWithInvalidFligthCodeShouldThrowException() throws AircraftException {
    	@SuppressWarnings("unused")
    	Aircraft a = new A380(null, 1, 0, 0, 0, 0 ){};
    }
    
    @Test(expected=AircraftException.class)
    public void constructorWithInvalidDepartureTimeThrowException() throws AircraftException {
    	@SuppressWarnings("unused")
		Aircraft a = new A380("", 0, 0, 0, 0, 0){};
    }  
    
    @Test(expected=AircraftException.class)
    public void constructorWithInvalidFirstClassThrowException() throws AircraftException {
        @SuppressWarnings("unused")
		Aircraft a = new A380("", 0, -1, 0, 0, 0){};
    } 
    
    @Test(expected=AircraftException.class)
    public void constructorWithInvalidBusinessClassThrowException() throws AircraftException {
        @SuppressWarnings("unused")
		Aircraft a = new A380("", 0, 0, -1, 0, 0){};
    } 
    
    @Test(expected=AircraftException.class)
    public void constructorWithInvalidPremiumClassThrowException() throws AircraftException {
        @SuppressWarnings("unused")
		Aircraft a = new A380("", 0, 0, 0, -1, 0){};
    } 
    
    @Test(expected=AircraftException.class)
    public void constructorWithInvalidEconomyClassThrowException() throws AircraftException {
        @SuppressWarnings("unused")
		Aircraft a = new A380("", 0, 0, 0, 0, -1){};
    } 
    
/************* Following are te test for abstract class Passenger ***********/
    
    @Test
    public void cancelShouldSetStatus() throws AircraftException, PassengerException {
        String lastStatus;
        Aircraft a = getAircratfInstance();
        Passenger p = new Economy(Constants.FIRST_FLIGHT, departureTime);
        
        a.confirmBooking(p, confirmationTime);
        a.cancelBooking(p, cancellationTime);
        lastStatus = a.getStatus(cancellationTime);
        
        assertEquals("|Y:C>N|",
        		lastStatus.substring(
        				lastStatus.length() - 8, lastStatus.length() - 1
        		));
    }
    
    
    @Test
    public void cancelFirstClassShouldSetStatusF() throws AircraftException, PassengerException {
        String lastStatus;
        Aircraft a = getAircratfInstance();
        Passenger p = new First(bookingTime, departureTime);
        
        a.confirmBooking(p, confirmationTime);
        a.cancelBooking(p, cancellationTime);
        lastStatus = a.getStatus(cancellationTime);
        
        assertEquals("|F:C>N|",
        		lastStatus.substring(
        				lastStatus.length() - 8, lastStatus.length() - 1
        		));
    }
    
    @Test
    public void cancelBusinessClassShouldSetStatusJ() throws AircraftException, PassengerException {
        String lastStatus;
        Aircraft a = getAircratfInstance();
        Passenger p = new Business(bookingTime, departureTime);
        
        a.confirmBooking(p, confirmationTime);
        a.cancelBooking(p, cancellationTime);
        lastStatus = a.getStatus(cancellationTime);
        
        assertEquals("|J:C>N|",
        		lastStatus.substring(
        				lastStatus.length() - 8, lastStatus.length() - 1
        		));
    }
    
    @Test
    public void cancelPremiumClassShouldSetStatusP() throws AircraftException, PassengerException {
        String lastStatus;
        Aircraft a = getAircratfInstance();
        Passenger p = new Premium(bookingTime, departureTime);
        
        a.confirmBooking(p, confirmationTime);
        a.cancelBooking(p, cancellationTime);
        lastStatus = a.getStatus(cancellationTime);
        
        assertEquals("|P:C>N|",
        		lastStatus.substring(
        				lastStatus.length() - 8, lastStatus.length() - 1
        		));
    }
    
    @Test
    public void confirmBookingShouldAddSeat() throws AircraftException, PassengerException {
        String lastStatus;
        Aircraft a = getAircratfInstance();
        Passenger p = new First(bookingTime, departureTime);
        
        a.confirmBooking(p, confirmationTime);
        lastStatus = a.getStatus(confirmationTime);
        
        assertEquals("|F:N/Q>C|",
        		lastStatus.substring(
        				lastStatus.length() - 10, lastStatus.length() - 1
        		));
    }
    
    @Test(expected=AircraftException.class)
    public void confirmBookingWithNoSeatAvailableShouldThrowException() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        for (int i = 0; i <= first; i++) {
        	Passenger p = new First(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }
    }

    @Test
    public void fligthFullWithFullPassengersShouldReturnTrue() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        
        for (int i = 0; i < first; i++) {
        	Passenger p = new First(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }

        for (int i = 0; i < business; i++) {
        	Passenger p = new Business(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }

        for (int i = 0; i < premium; i++) {
        	Passenger p = new Premium(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }

        for (int i = 0; i < economy; i++) {
        	Passenger p = new Economy(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }
        
        assertTrue(a.flightFull());
    }
    
    @Test
    public void fligthFullWithSemiFullPassengersShouldReturnFalse() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        
        Passenger pf = new First(bookingTime, departureTime);
        a.confirmBooking(pf, confirmationTime);

        Passenger pb = new Business(bookingTime, departureTime);
        a.confirmBooking(pb, confirmationTime);

        Passenger pp = new Premium(bookingTime, departureTime);
        a.confirmBooking(pp, confirmationTime);

        Passenger pe = new Economy(bookingTime, departureTime);
        a.confirmBooking(pe, confirmationTime);
        
        assertFalse(a.flightFull());
    }
    
    @Test
    public void fligthEmptyWithEmptyPassengersShouldReturnTrue() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        
        assertTrue(a.flightEmpty());
    }
    
    
    @Test
    public void fligthEmptyWithPassengersShouldReturnFalse() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        
        Passenger pf = new First(bookingTime, departureTime);
        a.confirmBooking(pf, confirmationTime);

        Passenger pb = new Business(bookingTime, departureTime);
        a.confirmBooking(pb, confirmationTime);

        Passenger pp = new Premium(bookingTime, departureTime);
        a.confirmBooking(pp, confirmationTime);

        Passenger pe = new Economy(bookingTime, departureTime);
        a.confirmBooking(pe, confirmationTime);
        
        assertFalse(a.flightEmpty());
    }
    
    @Test
    public void hasPassangerWithExistingPassangerShouldReturnTrue() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        Passenger p = new First(bookingTime, departureTime);
        a.confirmBooking(p, 1);
        
        assertTrue(a.hasPassenger(p));
    }
    
    @Test
    public void hasPassangerWithNonExistingPassangerShouldReturnTrue() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        Passenger pReal = new First(bookingTime, departureTime);
        a.confirmBooking(pReal, confirmationTime);
        Passenger pFake = new First(bookingTime, departureTime);
        
        assertFalse(a.hasPassenger(pFake));
    }
    
    @Test
    public void seatsAvailableWithSeatsAvailableShouldReturnTrue() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        Passenger pf = new First(bookingTime, departureTime);
        Passenger pb = new Business(bookingTime, departureTime);
        Passenger pp = new Premium(bookingTime, departureTime);
        Passenger pe = new Economy(bookingTime, departureTime);
        
        assertTrue(a.seatsAvailable(pf));
        assertTrue(a.seatsAvailable(pb));
        assertTrue(a.seatsAvailable(pp));
        assertTrue(a.seatsAvailable(pe));
    }
    
    @Test
    public void seatsAvailableWithNoSeatsAvailableShouldReturnFalse() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        Passenger pf = new First(bookingTime, departureTime);
        Passenger pb = new Business(bookingTime, departureTime);
        Passenger pp = new Premium(bookingTime, departureTime);
        Passenger pe = new Economy(bookingTime, departureTime);
        
        for (int i = 0; i < first; i++) {
        	Passenger p = new First(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }

        for (int i = 0; i < business; i++) {
        	Passenger p = new Business(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }

        for (int i = 0; i < premium; i++) {
        	Passenger p = new Premium(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }

        for (int i = 0; i < economy; i++) {
        	Passenger p = new Economy(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }
        
        assertFalse(a.seatsAvailable(pf));
        assertFalse(a.seatsAvailable(pb));
        assertFalse(a.seatsAvailable(pp));
        assertFalse(a.seatsAvailable(pe));
    }
    
    @Test
    public void upgradeWithAvailableSeatsShouldUpgrade() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        Business pb = new Business(bookingTime, departureTime);

        // fill up the first class with 1 seat left for testing passenger to upgrade
        for (int i = 0; i < first - 1; i++) {
        	Passenger p = new First(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }
        
        // fill up the business class
        for (int i = 0; i < business - 1; i++) {
        	Passenger p = new Business(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }

    	a.confirmBooking(pb, confirmationTime);
    	pb.cancelSeat(cancellationTime);
        pb.queuePassenger(queueTime, departureTime);
        a.upgradeBookings();

        assertEquals(business - 1, a.getNumBusiness());
        assertEquals(first, a.getNumFirst());
    }
    

    @Test
    public void upgradeWithNoAvailableSeatsShouldNotUpgrade() throws AircraftException, PassengerException {
        Aircraft a = getAircratfInstance();
        Business pb = new Business(bookingTime, departureTime);
        
        // fill up the first class
        for (int i = 0; i < first; i++) {
        	Passenger p = new First(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }
        
        // fill up the business class
        for (int i = 0; i < business - 1; i++) {
        	Passenger p = new Business(bookingTime, departureTime);
        	a.confirmBooking(p, confirmationTime);
        }
        
        a.confirmBooking(pb, confirmationTime);
    	pb.cancelSeat(cancellationTime);
        pb.queuePassenger(queueTime, departureTime);
        a.upgradeBookings();
        
        assertEquals(business, a.getNumBusiness());
        assertEquals(first, a.getNumFirst());
    }
    
    /**
     * Helper method to create a new aircraft for testing
     * @return a new A380 aircraft
     * @throws AircraftException (not expected)
     */
    private Aircraft getAircratfInstance() throws AircraftException {
        Aircraft a = new A380(flightCode, departureTime, first, business, premium, economy){};
        return a;
    }
    

}
