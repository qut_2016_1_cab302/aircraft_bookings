
package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.Economy;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

public class EconomyTest {
    int bookingTime = 2;
    int queueTime = bookingTime + 1;
    int departureTime = bookingTime + 5;
    
    @Test
    public void constructorShouldSetBookingTimeDepartureTimeAndPassID() throws PassengerException {
    	Passenger economy = new Economy(bookingTime, departureTime);
        
        assertEquals(bookingTime, economy.getBookingTime());
        assertEquals(departureTime, economy.getDepartureTime());
        assertEquals("Y", economy.getPassID().substring(0, 1));
    }
    
    @Test
    public void noSeatsMessageShouldIncludeEconomy() throws PassengerException {
    	Passenger economy = new Economy(bookingTime, departureTime);
    	
    	assertEquals("No seats available in Economy", economy.noSeatsMsg());
    }
    
    @Test
    public void upgradeShouldReturnFirstClassPassenger() throws PassengerException {
    	Passenger p = new Economy(bookingTime, departureTime);
    	p = p.upgrade();
        
        assertTrue(p instanceof Premium);
    }
    
/************* Following are te test for abstract class Passenger ***********/
    
    @Test
    public void constructorShouldSetFields() throws PassengerException {
        int bookingTime = 1;
        int departureTime = 2;
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        
        assertTrue(p.isNew());
        assertFalse(p.isConfirmed());
        assertFalse(p.isFlown());
        assertFalse(p.isQueued());
        assertFalse(p.isRefused());
        assertEquals(bookingTime, p.getBookingTime());
        assertEquals(departureTime, p.getDepartureTime());
    }
    
    @Test(expected=PassengerException.class)
    public void constructorWithInvalidBookingTimeShouldThrowException() throws PassengerException {
        int bookingTime = -1;
        int departureTime = 2;
        createPassengerInstance(bookingTime, departureTime);
    }

    @Test(expected=PassengerException.class)
    public void constructorWithNegativeDepartureTimeShouldThrowException() throws PassengerException {
        int bookingTime = 1;
        int departureTime = -1;
        createPassengerInstance(bookingTime, departureTime);
    }

    @Test(expected=PassengerException.class)
    public void constructorWithInvalidDepartureTimeShouldThrowException() throws PassengerException {
        int bookingTime = 3;
        int departureTime = bookingTime - 1;
        createPassengerInstance(bookingTime, departureTime);
    }
    
    @Test(expected=PassengerException.class)
    public void cancelWithNegativeCancelationShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.cancelSeat(-1);
    }

    @Test(expected=PassengerException.class)
    public void cancelWithInvalidCancelationShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.cancelSeat(departureTime + 1);
    }

    @Test(expected=PassengerException.class)
    public void cancelWithNewStatushouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.cancelSeat(2);
    }

    @Test(expected=PassengerException.class)
    public void cancelWithQueuedStatushouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.queuePassenger(queueTime, departureTime);
        p.cancelSeat(queueTime + 1);
    }

    @Test(expected=PassengerException.class)
    public void cancelWithRefusedStatushouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.refusePassenger(++timeDay);
        p.cancelSeat(++timeDay);
    }

    @Test(expected=PassengerException.class)
    public void cancelWithFlownStatushouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.flyPassenger(++timeDay);
        p.cancelSeat(++timeDay);
    }
    
    @Test
    public void cancelShouldSetBookingTimeAndStatus() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.confirmSeat(++timeDay, departureTime);
        p.cancelSeat(++timeDay);
        
        assertEquals(timeDay, p.getBookingTime());
        assertTrue(p.isNew());
        assertFalse(p.isConfirmed());
    }

    @Test(expected=PassengerException.class)
    public void confirmSeatWithNegativeTimeShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.confirmSeat(-1, departureTime);
    }

    @Test(expected=PassengerException.class)
    public void confirmSeatWithInvalidTimeShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.confirmSeat(departureTime + 1, departureTime);
    }    

    @Test(expected=PassengerException.class)
    public void confirmSeatWithConfirmedStatusShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.confirmSeat(++timeDay, departureTime);
        p.confirmSeat(++timeDay, departureTime);
    }

    @Test(expected=PassengerException.class)
    public void confirmSeatWithRefusedStatusShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.refusePassenger(++timeDay);
        p.confirmSeat(++timeDay, departureTime);
    }

    @Test(expected=PassengerException.class)
    public void confirmSeatWithFlownStatusShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.flyPassenger(++timeDay);
        p.confirmSeat(++timeDay, departureTime);
    }

    @Test
    public void confirmSeatShouldSetStatusAndTimes() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        int newDepartureTime = departureTime + 2;
        p.confirmSeat(++timeDay, newDepartureTime);
        
        assertFalse(p.isNew());
        assertTrue(p.isConfirmed());
        assertEquals(timeDay, p.getConfirmationTime());
        assertEquals(newDepartureTime, p.getDepartureTime());
    }

    @Test
    public void confirmSeatWithQueuedStatusShouldSetExitQueuedTime() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.queuePassenger(++timeDay, departureTime);
        p.confirmSeat(++timeDay, departureTime);
        
        assertEquals(timeDay, p.getExitQueueTime());
    }

    @Test(expected=PassengerException.class)
    public void flyPassangerWithNegativeDepartureTimeShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.confirmSeat(bookingTime + 1, departureTime);
        p.flyPassenger(-1);
    }

    @Test(expected=PassengerException.class)
    public void flyPassangerWithNewStatusShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.flyPassenger(departureTime);
    }

    @Test(expected=PassengerException.class)
    public void flyPassangerWithQueuedStatusShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.queuePassenger(++timeDay, departureTime);
        p.flyPassenger(1);
    }
    
    @Test(expected=PassengerException.class)
    public void flyPassangerWithRefusedStatusShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.refusePassenger(++timeDay);
        p.flyPassenger(++timeDay);
    }

    @Test(expected=PassengerException.class)
    public void flyPassangerWithFlownStatusShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.confirmSeat(++timeDay, departureTime);
        p.flyPassenger(++timeDay);
        p.flyPassenger(++timeDay);
    }

    @Test
    public void flyPassangerShouldSetStatusAndDepartureTime() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.confirmSeat(++timeDay, departureTime);
        p.flyPassenger(++timeDay);
        
        assertTrue(p.isFlown());
        assertFalse(p.isConfirmed());
        assertEquals(timeDay, p.getDepartureTime());
    }

    @Test(expected=PassengerException.class)
    public void queuePassengerWithNegativeQueueTimeShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.queuePassenger(-1, departureTime);
    }

    @Test(expected=PassengerException.class)
    public void queuePassengerWithInvalidQueueTimeShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.queuePassenger(departureTime + 1, departureTime);
    }

    @Test
    public void queuePassengerShouldSetStatusAndQueueTimes() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.queuePassenger(++timeDay, departureTime);
        
        assertTrue(p.isQueued());
        assertEquals(timeDay, p.getEnterQueueTime());
    }

    @Test(expected=PassengerException.class)
    public void refusePassengerWithNegativeRefusalTimeShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.refusePassenger(-1);
    }

    @Test(expected=PassengerException.class)
    public void refusePassengerWithInvalidRefusalTimeShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        p.refusePassenger(bookingTime - 1);
    }

    @Test(expected=PassengerException.class)
    public void refusePassangerWithConfirmedStatusShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.confirmSeat(++timeDay, departureTime);
        p.refusePassenger(++timeDay);
    }

    @Test(expected=PassengerException.class)
    public void refusePassangerWithRefusedStatusShouldThrowException() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.refusePassenger(++timeDay);
        p.refusePassenger(++timeDay);
    }

    @Test
    public void refusePassangerWithNewStatusShouldSwitchState() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.refusePassenger(++timeDay);
        
        assertFalse(p.isNew());
        assertTrue(p.isRefused());
    }

    @Test
    public void refusePassangerWithQueuedStatusShouldSwitchState() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.queuePassenger(++timeDay, departureTime);
        p.refusePassenger(departureTime);
        
        assertFalse(p.isQueued());
        assertTrue(p.isRefused());
    }

    @Test
    public void refusePassangerWithQueuedStatusShouldNotSwitchState() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.queuePassenger(++timeDay, departureTime);
        p.refusePassenger(++timeDay);
        
        assertFalse(p.isNew());
        assertFalse(p.isRefused());
        assertTrue(p.isQueued());
    }

    @Test
    public void copyPassangerShouldCopyAllFields() throws PassengerException {
        Passenger p = createPassengerInstance(bookingTime, departureTime);
        int timeDay = bookingTime;
        p.confirmSeat(++timeDay, departureTime);
        p.cancelSeat(++timeDay);
        p.queuePassenger(++timeDay, departureTime);
        
        Passenger np = p.upgrade();
        
        assertEquals(p.isNew(), np.isNew());
        assertEquals(p.isConfirmed(), np.isConfirmed());
        assertEquals(p.isQueued(), np.isQueued());
        assertEquals(p.isFlown(), np.isFlown());
        assertEquals(p.isRefused(), np.isRefused());
        assertEquals(p.getBookingTime(), np.getBookingTime());
        assertEquals(p.getEnterQueueTime(), np.getEnterQueueTime());
        assertEquals(p.getExitQueueTime(), np.getExitQueueTime());
        assertEquals(p.getConfirmationTime(), np.getConfirmationTime());
        assertEquals(p.getDepartureTime(), np.getDepartureTime());
    }
    
    /**
     * Helper method to create a new passenger for testing
     * @return a new economy class passenger
     * @throws PassengerException (not expected)
     */
    private Passenger createPassengerInstance(int bookingTime, int departureTime) throws PassengerException {
        return new Economy(bookingTime, departureTime) {
            
            @Override
            public Passenger upgrade() {
                return null;
            }
            
            @Override
            public String noSeatsMsg() {
                return null;
            }
        };
    }
}
