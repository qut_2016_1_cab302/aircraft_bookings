package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Aircraft.Bookings;

public class BookingsTest {
    
    @Test
    public void constructorShouldSetFields() {
        int numEconomy=10;
        int total=100;
        int numFirst=20;
        int available=2;
        int numPremium=5;
        int numBusiness=8;
        Bookings bookings = new Bookings(numFirst, numBusiness, numPremium, numEconomy, total, available);
        
        assertEquals(numFirst, bookings.getNumFirst());
        assertEquals(numBusiness, bookings.getNumBusiness());
        assertEquals(numPremium, bookings.getNumPremium());
        assertEquals(numEconomy, bookings.getNumEconomy());
        assertEquals(total, bookings.getTotal());
        assertEquals(available, bookings.getAvailable());
    }
}
