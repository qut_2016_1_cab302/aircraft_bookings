
package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

public class PremiumTest {
    int bookingTime = 2;
    int departureTime = 5;
    
    @Test
    public void constructorShouldSetBookingTimeDepartureTimeAndPassID() throws PassengerException {
        Passenger premium = new Premium(bookingTime, departureTime);
        
        assertEquals(bookingTime, premium.getBookingTime());
        assertEquals(departureTime, premium.getDepartureTime());
        assertEquals("P", premium.getPassID().substring(0, 1));
    }
    
    @Test
    public void noSeatsMessageShouldIncludePremium() throws PassengerException {
    	Passenger business = new Premium(bookingTime, departureTime);
    	
    	assertEquals("No seats available in Premium", business.noSeatsMsg());
    }
    
    @Test
    public void upgradeShouldReturnFirstClassPassenger() throws PassengerException {
    	Passenger p = new Premium(bookingTime, departureTime);
    	p = p.upgrade();
        
        assertTrue(p instanceof Business);
    }
}
