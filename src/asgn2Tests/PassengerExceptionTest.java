package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Passengers.PassengerException;

public class PassengerExceptionTest{

    @Test
    public void exceptionShouldSetMessage() {
    	String exceptionStartWith = "Passenger Exception: ";
    	String exceptionendWith = "msj";
        PassengerException pe = new PassengerException("msj");
        String errorMessage = pe.getMessage();
        
        assertEquals(exceptionStartWith,
        		errorMessage.substring(0,
        								  exceptionStartWith.length()));
        assertEquals(exceptionendWith,
        		errorMessage.substring(errorMessage.length() - exceptionendWith.length(),
        								errorMessage.length()));
    }
}
