package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Simulators.SimulationException;

public class SimulationExceptionTest {

    @Test
    public void exceptionShouldSetMessage() {
    	String exceptionStartWith = "Simulation Exception: ";
    	String exceptionendWith = "msj";
    	SimulationException se = new SimulationException("msj");
        String errorMessage = se.getMessage();
        
        assertEquals(exceptionStartWith,
        		errorMessage.substring(0,
        								  exceptionStartWith.length()));
        assertEquals(exceptionendWith,
        		errorMessage.substring(errorMessage.length() - exceptionendWith.length(),
        								errorMessage.length()));
    }
}
