
package asgn2Tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

public class BusinessTest {
    int bookingTime = 2;
    int departureTime = 5;
    
    @Test
    public void constructorShouldSetBookingTimeDepartureTimeAndPassID() throws PassengerException {
    	Passenger business = new Business(bookingTime, departureTime);
        
        assertEquals(bookingTime, business.getBookingTime());
        assertEquals(departureTime, business.getDepartureTime());
        assertEquals("J", business.getPassID().substring(0, 1));
    }
    
    @Test
    public void noSeatsMessageShouldIncludeBusiness() throws PassengerException {
    	Passenger business = new Business(bookingTime, departureTime);
    	
    	assertEquals("No seats available in Business", business.noSeatsMsg());
    }
    
    @Test
    public void upgradeShouldReturnFirstClassPassenger() throws PassengerException {
    	Passenger p = new Business(bookingTime, departureTime);
    	p = p.upgrade();
        
        assertTrue(p instanceof First);
    }
}
